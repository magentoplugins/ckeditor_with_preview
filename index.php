<html>
<head>
	<title>Sample CKEditor Site</title>
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var divContents = $("#contents_preview").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();            
        });
    </script>
</head>
<body>
	<textarea id="contents" name="contents">This is an editor</textarea></p>
<div id="contents_preview" style="display:none;"></div>
<input type="button" id="btnPrint" value="Preview" />
<script type='text/javascript'>

CKEDITOR.on('instanceCreated', function (e) {
    document.getElementById( e.editor.name + '_preview').innerHTML = e.editor.getData();
    e.editor.on('change', function (ev) {
        document.getElementById( ev.editor.name + '_preview').innerHTML = ev.editor.getData();
    });
});
var config = { extraPlugins: 'onchange'};
CKEDITOR.replace('contents', config);


</script>
</body>
</html>


